from pickle import TRUE
from django.db import models

# Create your models here.
class Tododetails(models.Model):
    id = models.IntegerField(primary_key=True,default=True)
    taskname = models.CharField(max_length = 100)
    taskid = models.IntegerField(null=True)
    taskdate = models.DateField(max_length=100)
    taskweek = models.IntegerField(null=True)
    tasktime = models.TimeField(null=True)
    taskendtime = models.TimeField(null=True)
