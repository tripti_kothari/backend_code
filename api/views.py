from asyncore import read
from cgitb import lookup
from gzip import READ
import json
from pickle import PUT, TRUE
from re import search
from django.http import JsonResponse, request
from django.http import QueryDict
from django.shortcuts import render
from rest_framework.response import Response
from .serializers import TodoSerializer
from .models import Tododetails
from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.generics import UpdateAPIView
from rest_framework.generics import DestroyAPIView
from rest_framework.generics import GenericAPIView

from rest_framework import filters
from rest_framework import status


from api import serializers

class TododetailsList(GenericAPIView):
    
    allowed_methods = ['GET','POST','PUT','DELETE']

    def get (self, request):
         queryset = Tododetails.objects.all()
         serializer= TodoSerializer(queryset, many= True)
         return Response({"status": True, "data":serializer.data})
        
    # def filter(self, request):
    #     filter_backends = ['taskweek']
    #     # SearchFilter = ['taskweek']
    #     serializer= TodoSerializer(filter_backends, many = True)
    #     return Response({"status": True, "data":serializer.data})

    def post (self, request,*args, **kwargs):
        serializer = TodoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": True, "data": serializer.data}, status=status.HTTP_200_OK)
        return Response({"status": False, "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def put (self, request):
        serializer = Tododetails(data = request.get(id=pk))
        if serializer.is_valid():
            serializer.save()
            print(request.get())
            return Response({"status": True, "data": serializer.data}, status=status.HTTP_200_OK)
        return Response({"status": False, "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


    def delete():
        serializer = Tododetails(data = request.get({pk}))
        if serializer.is_valid():
            serializer.save()
            return Response({"status": True, "data": serializer.data}, status=status.HTTP_200_OK)
        return Response({"status": False, "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


    
# # Create your views here.
# class TododetailsList(ListAPIView, CreateAPIView):
#     queryset = Tododetails.objects.all()
#     serializer_class = TodoSerializer 
#     filter_backends = [filters.SearchFilter]   
#     search_fields = ['taskweek']  
    
# # def getdata(request):
# #     if request.method == 'GET':
# #         queryset = Tododetails.objects.all()


# def fetchdetails (request):
#     if request.method == 'POST':
#         #Tododetails.taskname = request.POST['id']
#         Tododetails.taskname  = request.POST['taskname']
#         Tododetails.taskid  = request.POST['taskid']
#         Tododetails.taskdate  = request.POST['taskdate']
#         Tododetails.taskweek = request.POST['taskweek']
#         Tododetails.tasktime = request.POST['tasktime']
#         Tododetails.taskendtime = request.POST['taskendtime']
#         return Response("data has been added")
#     else:
#         return Response("data does not get enter")



# class todoput(UpdateAPIView,DestroyAPIView):
#     queryset = Tododetails
#     serializer_class = TodoSerializer 
       
#####
# def tutorial_detail(request,self,pk):
#     try:
#         if request.method == "PUT":
#             Todo_data = json.loads(request.body)
#             data = Tododetails.objects.filter(pk = Todo_data.get("id"))
#             data.taskname = Todo_data.get('taskname')
#             data.taskid = Todo_data.get('taskid')
#             data.tasdate = Todo_data.get('taskdate')
#             data.taskweek = Todo_data.get('taskweek')
#             return Tododetails.objects.filter(id=self.request.data)
#         else:
#             return JsonResponse("cannot be updated")

#     except Tododetails.DoesNotExist:
#         return JsonResponse("bad request")

    
# def delete_details(pk):
#     read_only = True
#     a =  Tododetails.objects.filter['id']
#     a.delete()
#     return Response("data has been delete")







    # try: 
    #     tutorial = Tododetails.objects.get(pk=pk) 
    # except Tododetails.DoesNotExist: 
    #     return JsonResponse({'message': 'The tutorial does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
    # if request.method == 'PUT': 
    #     tutorial_data = json().parse(request) 
    #     tutorial_serializer = TodoSerializer(tutorial, data=tutorial_data) 
    #     if tutorial_serializer.is_valid(): 
    #         tutorial_serializer.save() 
    #         return JsonResponse(tutorial_serializer.data) 
    #     return JsonResponse(tutorial_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 


# def put(request):
#         #todo_data = json.loads(request.body.decode('utf8'))
#         put = QueryDict(request.body)
#         if request.method == 'PUT':
#             a = Tododetails.objects.filter(pk=put.get("id"))
#             #a = Tododetails.objects.get(id=put.get("id"))
#             a.taskname = Tododetails.POST("taskname")
#             a.taskid = Tododetails.POST("taskid")
#             a.taskdate = Tododetails.POST("taskdate")
#             a.taskweek = Tododetails.POST("taskweek")
#             a.save()
#             Todo_serializer = TodoSerializer(a)
#             return JsonResponse({"success": True, "data": Todo_serializer.data}, safe = False)

# # def patch(self,request,id=None):
# #         todo_data = json.loads(request.body.decode('utf8'))
# #         if id:
# #             try:
#              a = Tododetails.objects.get(pk=int(id))
#              a.taskdate = todo_data.get("taskdate")
#              a.save()
#             except a.DoesNotExist:
#                 print("No record found")
#                 return JsonResponse({"success": False, "message": "student not found"}, safe = False)  

#         todo_serializer = TodoSerializer(a)
#         return JsonResponse({"success": True, "message": "student is updated Successfully", "data": todo_serializer.data}, safe = False)



# def updatedetails (request,pk,status):
#     try:
#         tododata = Tododetails.objects.get(pk=pk)
#     except Tododetails.DoesNotExist:
#         return JsonResponse({'message': 'tutorial does not get exist '},status = status.HTTP_404_NOT_FOUND)
#     if request.method == 'PUT':
#         tododata = JSONParser().parse(request)
#         Todo_Serializer = TodoSerializer(data=tododata)
#         if Todo_Serializer.is_valid():
#             Todo_Serializer.save()
#             return JsonResponse(Todo_Serializer.data)
#         return JsonResponse(Todo_Serializer.errors)

    #     put = QueryDict(request.body)
    #     a = Tododetails.objects.get(id = put.get("id"))
    #     return Response("data has get updated")
    # else:
    #     return Response("data does not updated")
    




    # if request.method == 'PUT':
    #     a = PUT.get(QueryDict)
    #     queryset = Tododetails.objects.get(id = a(id,pk))
    #     queryset.taskdate = request.data['taskdate']
    #     queryset.save()
    #     return Response("update details are")
    # else:
    #     return Response("data does not get update")




    # details = Tododetails.objects.get(taskid=pk)
    # getdetails = TodoSerializer(instance=details)

# def filteringdata (request):
#         # Tododetails.taskweek = request.POST['taskweek']
#         queryset = Tododetails.objects.all
#         queryset = queryset.filters(Tododetails.taskweek)
#         return Response("data after filtering")



        #         taskname  = request.POST['taskname']
        # taskid  = request.POST['taskid']
        # taskdate  = request.POST['taskdate']
        # print (taskname, taskid, taskdate)
        #   serializer_class = TodoSerializer(data = request.data)
        # if serializer_class.is_valid():
        #     serializer_class.save()
        #     return Response(serializer_class.data)

    #     or index, record in df.iterrows():
    #  movie_obj = MovieData()
    #  movie_obj.title = record['title']
    #  ....
    #  movie_obj.save()

    # if (request.method == 'POST'):
    #     queryset  = Tododetails.objects.create(Tododetails.taskname)
    #     queryset.save()
    #     return Response("data has been added")
    # else:
    #     return Response("data does not get enter")
