# Generated by Django 4.0.1 on 2022-01-28 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_alter_tododetails_taskid'),
    ]

    operations = [
        migrations.AddField(
            model_name='tododetails',
            name='taskweek',
            field=models.IntegerField(max_length=100, null=True),
        ),
    ]
