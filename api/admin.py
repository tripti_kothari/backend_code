from django.contrib import admin
from .models import Tododetails
# Register your models here.
@admin.register(Tododetails)
class TodoAdmin(admin.ModelAdmin):
    list_display = ['id','taskname','taskid','taskdate','taskweek','tasktime','taskendtime']