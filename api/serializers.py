from dataclasses import fields
from django.http import request
from httplib2 import Response
from rest_framework import serializers
from .models import Tododetails
class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tododetails
        fields =  ['id','taskname','taskid','taskdate','taskweek','tasktime','taskendtime'] 
        
        #'_all_'


# def fetchdetails (request):
#     if request.method == 'POST':
#         Tododetails.taskname  = request.POST['taskname']
#         Tododetails.taskid  = request.POST['taskid']
#         Tododetails.taskdate  = request.POST['taskdate']
#         return Response("data has been added")
#     else:
#         return Response("data does not get enter")
